import RPi.GPIO as GPIO
import time
import tkinter as TK

def redLed():
    GPIO.output(red, True)
    GPIO.output(yellow, False)
    GPIO.output(green, False)

def yellowLed():
    GPIO.output(red, False)
    GPIO.output(yellow, True)
    GPIO.output(green, False)

def greenLed():
    GPIO.output(red, False)
    GPIO.output(yellow, False)
    GPIO.output(green, True)

# Declaring Variables
red = 7
yellow = 11
green = 13

# Setting up GPIO pins
GPIO.setmode(GPIO.BOARD)
GPIO.setup(red, GPIO.OUT)
GPIO.setup(yellow, GPIO.OUT)
GPIO.setup(green, GPIO.OUT)

# Setting up GUI
win = TK.Tk()
win.title("Turn on LEDs")

redButton = TK.Button(win, text = "Turn on Red LED", command = redLed)
redButton.pack()

yellowButton = TK.Button(win, text = "Turn on Yellow LED", command = yellowLed)
yellowButton.pack()

greenButton = TK.Button(win, text = "Turn on Green LED", command = greenLed)
greenButton.pack()

win.mainloop()

# Clean up GPIO pins after use
GPIO.cleanup()

